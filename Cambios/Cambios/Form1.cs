﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using Cambios.Modelos;

namespace Cambios
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadRates();
        }

        private async void LoadRates()
        {

            bool load;
            var cliente = new HttpClient();
            cliente.BaseAddress = new Uri("http://apiexchangerates.azurewebsites.net");
            HttpResponseMessage response =  await cliente.GetAsync("/api/Rates");
            string result = await response.Content.ReadAsStringAsync();

            if (!response.IsSuccessStatusCode)
            {
                MessageBox.Show(response.ReasonPhrase);
                return;
            }
            var rates = JsonConvert.DeserializeObject<List<Rate>>(result);

            //Corrigir bug da microsoft 
        
            foreach (var item in rates)
            {
                ComboBoxOrigem.Items.Add(item.Name);
                ComboBoxDestino.Items.Add(item.Name);
            }
            ComboBoxOrigem.SelectedIndex = 0;
            ComboBoxDestino.SelectedIndex = 0;
            /* 
            ComboBoxOrigem.DataSource = rates;
            ComboBoxOrigem.DisplayMember = "Name";
            ComboBoxDestino.BindingContext = new BindingContext();
            ComboBoxDestino.DataSource = rates;
            ComboBoxDestino.DisplayMember = "Name"; 
            */
            ProgressBar1.Value =100;

        }
    }
}
